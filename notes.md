| Operator | Meaning                                                      | Example  | Result |
| -------- | ------------------------------------------------------------ | -------- | ------ |
| `+`      | Add two operands or unary plus.                              | `1 + 1`  | `2`    |
| `-`      | Substract right operand from the left operand or unary minus. | `2 - 4`  | `-2`   |
| `*`      | Multiply two operands.                                       | `3 * 4`  |        |
| `/`      | Divide left operand by the right operand (the results is `float`). | `3 / 4`  |        |
| `//`     | Integer division.                                            | `3 // 4` |        |
| `%`      | Modulus (remainder of integer division)                      | `3 % 4`  |        |
| `**`     | Exponentiation.                                              | `3 ** 4` |        |

