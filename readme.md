# About

This repository contains my notes about Python 3.





- Comments
- Basic `print`
- Math operations (`int`, `float`, and boolean data types)
- Lists (add, remove, search items, list comprehension)
- Tuples
- Dictionaries (add, remove, search items, dictionary comprehension)
- Strings
- Control flow (`for`, `while`, `if`, `else`, `enumerate`, `range`)
- File handling (read/write text files)
- Functions (`def`, `lambda`)
- Modules (`import`)
- Classes



**Advanced**

- Generators
- Decorators
- Flask
- Numpy



## Contact

Auraham Camacho `auraham.cg@gmail.com`